variable "az" {
  default = ["eu-west-3a", "eu-west-3b"]
}

variable "dns_zone" {
    type = string
    default = "equusasinus.com"
}

variable "front_record_name" {
    type = string
    default = "equus"
}
